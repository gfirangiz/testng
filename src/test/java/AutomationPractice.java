import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class AutomationPractice {
    WebDriver driver;

    @BeforeMethod
    public void setUp(){
        driver = new ChromeDriver();
        driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }
    @Test
    public void checkLogo(){
        WebElement logo = driver.findElement(By.cssSelector(".orangehrm-login-branding>img"));
        Assert.assertEquals(logo.getAttribute("alt"),"company-branding");
    }
    @Test
    public void loginFailureCase() {
        WebElement loginSpace = driver.findElement(By.cssSelector("div>input[name='username']"));
        loginSpace.sendKeys("nsajnj");
        WebElement passwordSpace = driver.findElement(By.cssSelector("div>input[name='password']"));
        passwordSpace.sendKeys("vbjvnjnd");
        WebElement loginButton = driver.findElement(By.cssSelector("button[type='submit']"));
        loginButton.click();
        WebElement allertMessage = driver.findElement(By.cssSelector("div.oxd-alert-content.oxd-alert-content--error > p"));
        Assert.assertTrue(allertMessage.isDisplayed());
    }
    @Test
    public void loginSuccessCase(){
        WebElement loginSpace = driver.findElement(By.cssSelector("div>input[name='username']"));
        loginSpace.sendKeys("Admin");
        WebElement passwordSpace = driver.findElement(By.cssSelector("div>input[name='password']"));
        passwordSpace.sendKeys("admin123");
        WebElement loginButton = driver.findElement(By.cssSelector("button[type='submit']"));
        loginButton.click();
        List<WebElement> sideBar = driver.findElements(By.cssSelector("ul.oxd-main-menu>li"));
        Assert.assertEquals(sideBar.get(7).getText(),"Dashboard","wrong message");
    }

    /* a[class='oxd-main-menu-item active'] dashboard elementin css */
    @Test
    public void helpFunctionallityChecking() {
        WebElement loginSpace = driver.findElement(By.cssSelector("div>input[name='username']"));
        loginSpace.sendKeys("Admin");
        WebElement passwordSpace = driver.findElement(By.cssSelector("div>input[name='password']"));
        passwordSpace.sendKeys("admin123");
        WebElement loginButton = driver.findElement(By.cssSelector("button[type='submit']"));
        loginButton.click();
        List<WebElement> sideBar = driver.findElements(By.cssSelector("ul.oxd-main-menu>li"));
        sideBar.get(0).click();
        WebElement helpIcon = driver.findElement(By.cssSelector("button>i[class='oxd-icon bi-question-lg']"));
        helpIcon.click();
        ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        WebElement headerText = driver.findElement(By.cssSelector("header>h1"));
        Assert.assertEquals(headerText.getText(), "How to Add a User Account", "wrong text");
        driver.close();
    }

        @AfterMethod
        public void tearDown () {
            driver.quit();
        }
    }
