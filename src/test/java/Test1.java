import org.testng.annotations.*;

public class Test1 {
    @BeforeGroups("a")
    public void groupSetUp(){
        System.out.println("Before group A");
    }
    @BeforeSuite
    public void SuiteSetUp() {
        System.out.println("Run this for suite");
    }
    @BeforeClass
    public void classSetUp() {
        System.out.println("Run it in this class");
    }

    @BeforeTest
    public void testSetUp() {
        System.out.println("Run it before test");
    }

    @BeforeMethod
    public void setUp() {
        System.out.println("Run it before methods");
    }

    @Test(groups = "a",priority = 0, description = "test1", alwaysRun = true, dependsOnMethods = "test1")
    public void test1() {
        System.out.println("Hello");
    }

    @Test(groups = "a",invocationCount = 3,enabled = false) /* invocationCount - nece defe run etmek isteyirsense, enabled false olduqda run etmir*/
    public void test2() {
        System.out.println("Hello 2");
    }

    @AfterMethod
    public void tearDown(){
        System.out.println("Exit from method");
    }

    @AfterGroups
    public void groupExit(){
        System.out.println("Exit from group");
    }

    @AfterClass
    public void classExit(){
        System.out.println("Exit from class");
    }

    @AfterTest
    public void testExit() {
        System.out.println("Exit from test");
    }

    @AfterSuite
    public void suiteExit(){
        System.out.println("Exit after suite");
    }
}
