import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import java.time.Duration;
import java.util.List;

public class Homework {
    WebDriver driver;

    JavascriptExecutor js = (JavascriptExecutor) driver;

    @BeforeMethod
    public void setUp(){
        driver = new ChromeDriver();
        driver.get("https://demoqa.com/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }

    @Test
    public void checkTitle() {
        WebElement title = driver.findElement(By.cssSelector("a[href='https://demoqa.com']"));
        Assert.assertTrue(title.isDisplayed());
    }

    @Test
    public void verifyingEnteredData() {
        WebElement elementsIcon = driver.findElement(By.cssSelector("div.category-cards>div:nth-child(1)"));
        elementsIcon.click();
        WebElement textBox = driver.findElement(By.cssSelector("ul.menu-list>li[id='item-0']"));
        textBox.click();
        WebElement userName = driver.findElement(By.cssSelector("input[id='userName']"));
        userName.sendKeys("Leyla");
        WebElement email = driver.findElement(By.cssSelector("input[id='userEmail']"));
        email.sendKeys("leyla@mail.ru");
        WebElement currentAddress = driver.findElement(By.cssSelector("div>[id='currentAddress']"));
        currentAddress.sendKeys("Baku,Yasamal");
        WebElement permanentAddress = driver.findElement(By.cssSelector("div>[id='permanentAddress']"));
        permanentAddress.sendKeys("Baku,Bayil");
        Actions action = new Actions(driver);
        action.sendKeys(Keys.PAGE_DOWN).perform();
        WebElement submitButton = driver.findElement(By.cssSelector("div>div>button[id='submit']"));
        WebDriverWait wait = new WebDriverWait(driver,(Duration.ofSeconds(5)));
        wait.until(ExpectedConditions.elementToBeClickable(submitButton));
        submitButton.click();
        WebElement viewName = driver.findElement(By.cssSelector("div>p[id='name']"));
        Assert.assertEquals(viewName.getText(),"Name:Leyla","Wrong name.");
        WebElement viewEmail = driver.findElement(By.cssSelector("div>p[id='email']"));
        Assert.assertEquals(viewEmail.getText(),"Email:leyla@mail.ru","Wrong email.");
        WebElement viewCurrentAddress = driver.findElement(By.cssSelector("div>p[id='currentAddress']"));
        Assert.assertEquals(viewCurrentAddress.getText(),"Current Address :Baku,Yasamal","Wrong current address.");
        WebElement viewPermanentAddress = driver.findElement(By.cssSelector("div>p[id='permanentAddress']"));
        Assert.assertEquals(viewPermanentAddress.getText(),"Permananet Address :Baku,Bayil","Wrong permanent address.");
    }

    @Test
    public void chooseNotesFromCheckBox() {
        WebElement elementsIcon = driver.findElement(By.cssSelector("div.category-cards>div:nth-child(1)"));
        elementsIcon.click();
        WebElement checkBox = driver.findElement(By.cssSelector("ul.menu-list>li[id='item-1']"));
        checkBox.click();
        WebElement homeCheckBox = driver.findElement(By.cssSelector("div[id='tree-node'] > ol > li > span > button > svg"));
        homeCheckBox.click();
        WebElement desktop = driver.findElement(By.cssSelector("div[id='tree-node'] > ol > li > ol > li:nth-child(1) > span > button > svg"));
        WebDriverWait wait = new WebDriverWait(driver,(Duration.ofSeconds(5)));
        wait.until(ExpectedConditions.elementToBeClickable(desktop));
        desktop.click();
        WebElement notes = driver.findElement(By.cssSelector("label[for='tree-node-notes']> span.rct-checkbox > svg"));
        wait.until(ExpectedConditions.elementToBeClickable(notes));
        notes.click();
        WebElement isChecked = driver.findElement(By.cssSelector("label[for='tree-node-notes']> span.rct-checkbox>svg.rct-icon.rct-icon-check"));
        Assert.assertEquals(isChecked.getAttribute("class"),"rct-icon rct-icon-check","Wrong text.");
    }

    @Test
    public void isButtonClickedChecking(){
        WebElement elementsIcon = driver.findElement(By.cssSelector("div.category-cards>div:nth-child(1)"));
        elementsIcon.click();
        WebElement buttons = driver.findElement(By.cssSelector(".element-group:nth-child(1)>div>ul>li:nth-child(5)"));
        buttons.click();
        WebElement rightClickMeButton = driver.findElement(By.cssSelector("button[id='rightClickBtn']"));
        rightClickMeButton.click();
    }


    @AfterMethod
    public void tearDown () {
        driver.quit();
    }
}
