import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class TestGoogle {
    WebDriver driver;

    @BeforeMethod
    public void setUp(){
        driver = new ChromeDriver();
        driver.get("https://www.google.com/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }

    @Test
    public void checkGoogleSearch(){
        WebElement searchInput = driver.findElement(By.cssSelector("#APjFqb"));
        searchInput.sendKeys("selenium");
        Assert.assertEquals(searchInput.getAttribute("value"),"selenium");
    }
    @AfterMethod
    public void tearDown(){
        driver.quit();
    }
}