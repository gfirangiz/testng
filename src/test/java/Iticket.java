import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.List;

public class Iticket {
    WebDriver driver;

    @BeforeMethod
    public void setUp() {
        driver = new ChromeDriver();
        driver.get("https://iticket.az/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }

    @Test
    public void selectEventPlace() {
        WebElement konsertTab = driver.findElement(By.cssSelector("span>a[href=\"/en/events/concerts\"]"));
        konsertTab.click();
        WebElement eventPlaceSelectBox = driver.findElement(By.cssSelector(".filter>div>.ts-control"));
        eventPlaceSelectBox.click();
        List<WebElement> eventPlace = driver.findElements(By.cssSelector("div[role='listbox']>div"));
        eventPlace.get(1).click();
    }
    @AfterMethod
    public void tearDown(){
        driver.quit();
    }
}
